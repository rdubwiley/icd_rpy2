from rpy2.robjects.vectors import DataFrame
from rpy2.robjects.packages import importr, data

r_base = importr('base')

utils = importr("utils")
package_name = "icd"

utils.install_packages(package_name)