import pandas as pd
from rpy2.robjects.vectors import DataFrame
from rpy2.robjects.packages import importr, data
from rpy2.robjects import pandas2ri
import rpy2.robjects as ro
from rpy2.robjects.conversion import localconverter

base = importr('base')
icd = importr('icd')

pandas2ri.activate()

df = pd.read_csv('test_data.csv')

print(icd.comorbid_charlson(df, icd_name='icd9', return_df=True))